FROM mediawiki:1.31.1

RUN apt-get update && \
    apt-get install -y --no-install-recommends git zip libxml2-dev

# Setup chameleon skin with composer
RUN curl --silent --show-error https://getcomposer.org/installer | php

RUN php composer.phar require mediawiki/chameleon-skin "1.7.1"

# VisualEditor extension
RUN curl -s -o /tmp/extension-visualeditor.tar.gz https://extdist.wmflabs.org/dist/extensions/VisualEditor-${MEDIAWIKI_BRANCH}-`curl -s https://extdist.wmflabs.org/dist/extensions/ | grep -o -P "(?<=VisualEditor-${MEDIAWIKI_BRANCH}-)[0-9a-z]{7}(?=.tar.gz)" | head -1`.tar.gz && \
    tar -xzf /tmp/extension-visualeditor.tar.gz -C /var/www/html/extensions && \
    rm /tmp/extension-visualeditor.tar.gz

# PluggableAuth, requirement for OpenID (Auth0)
RUN curl -s -o /tmp/extension-pluggableauth.tar.gz https://extdist.wmflabs.org/dist/extensions/PluggableAuth-${MEDIAWIKI_BRANCH}-`curl -s https://extdist.wmflabs.org/dist/extensions/ | grep -o -P "(?<=PluggableAuth-${MEDIAWIKI_BRANCH}-)[0-9a-z]{7}(?=.tar.gz)" | head -1`.tar.gz && \
    tar -xzf /tmp/extension-pluggableauth.tar.gz -C /var/www/html/extensions && \
    rm /tmp/extension-pluggableauth.tar.gz

# OpenIDConnect, first grabbing requirements through composer
RUN php composer.phar require jumbojett/openid-connect-php

RUN curl -s -o /tmp/extension-openid.tar.gz https://extdist.wmflabs.org/dist/extensions/OpenIDConnect-${MEDIAWIKI_BRANCH}-`curl -s https://extdist.wmflabs.org/dist/extensions/ | grep -o -P "(?<=OpenIDConnect-${MEDIAWIKI_BRANCH}-)[0-9a-z]{7}(?=.tar.gz)" | head -1`.tar.gz && \
    tar -xzf /tmp/extension-openid.tar.gz -C /var/www/html/extensions && \
    rm /tmp/extension-openid.tar.gz
